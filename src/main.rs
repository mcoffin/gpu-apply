use std::{
    env,
    io::{
        self,
        Write,
    },
    fs,
};
fn do_command<S: AsRef<str>>(command: S) -> io::Result<()> {
    let write_path = "pp_od_clk_voltage";
    let s = format!("{}\n", command.as_ref());
    let mut file = fs::OpenOptions::new()
        .create(true)
        .write(true)
        .open(write_path)?;
    println!("applying command: {:?}", &s);
    file.write(s.as_bytes())?;
    file.flush()
}
fn main() {
    let mut command_count: usize = 0;
    for arg in env::args().skip(1) {
        do_command(arg).expect("Failed to write command");
        command_count = command_count + 1;
    }
    if command_count > 0 {
        do_command("c").expect("Failed to write commit command");
    }
    println!("done");
}
